#!/usr/bin/python3
#
# Copyright (C) 2019 Oleg Shnaydman
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#


import os
import argparse
import requests
import json
import re

DROPBOX_ERROR_CODE = 1
ZAPIER_ERROR_CODE = 2
TEMPLATE_ERROR_CODE = 3
CHANGES_ERROR_CODE = 4
OUTPUT_FILE_PARSING_ERROR = 5

DROPBOX_UPLOAD_ARGS = {
    'path': None,
    'mode': 'overwrite',
    'autorename': True,
    'strict_conflict': True
}
DROPBOX_UPLOAD_URL = 'https://content.dropboxapi.com/2/files/upload'

DROPBOX_SHARE_DATA = {
    'path': None,
    'settings': {
        'requested_visibility': 'public'
    }
}
DROPBOX_SHARE_URL = 'https://api.dropboxapi.com/2/sharing/create_shared_link_with_settings'

DROPBOX_DELETE_DATA = {
    'path' : None
}
DROPBOX_DELETE_URL = 'https://api.dropboxapi.com/2/files/delete_v2'

ZAPIER_SEND_DATA = {
    'to': None,
    'subject': None,
    'body': None
}

def upload_to_itms(url,source_file,path_img,rootAgency,models,ageNames,appDescription,versionDescription,appType):
       
#    try:
#        _create_unverified_https_context = ssl._create_unverified_context
#    except AttributeError:
        # Legacy Python that doesn't verify HTTPS certificates by default
#        pass
#    else:
        # Handle target environment that doesn't support HTTPS verification
#        ssl._create_default_https_context = _create_unverified_https_context

    #url = "https://114.199.86.218:9443/iss/app/upload"
    #url = "https://itms-stg.spots.co.id/iss/app/upload"

    payload = {'rootAgency': rootAgency,
                'models': models,
                'ageNames': ageNames,
                'appDescription': appDescription,
                'versionDescription': versionDescription,
                'appType': appType
                }
    
    #fields = [('apk', ('app-release-1.1.9.apk', open(source_file,'rb'), "application/vnd.android.package-archive")),
    #          ('screenShot', ('test.jpeg', open(path_img,'rb'), "image/jpeg"))
    #        ]
        
    files = [
       ('apk', open(source_file,'rb')),
       ('screenShot', open(path_img,'rb'))
    ]
    
    headers = {
      #'Content-Type': 'multipart/form-data' ## command this header for solve for upload file
    }
    response = requests.request("POST", url, headers=headers, data = payload, files = files,verify=False)
         
    if response.status_code != requests.codes.ok:
        print("ResponseCode : ".format(errcode=response.status_code)) 
        return response.text.encode('utf8')
    
    resp = json.loads(str(response.text))
    
    #data = {"reviewId": resp['data'],
    #        "categoryName": "General App",
    #        "reviewResult": 1,
    #        "note": "Agree"}
    #headers_ = {
    #  'Content-Type': 'application/json'
    #}    
    url_ = "https://114.199.86.218:9443/iss/app/review"
    note = "Auto Approve by gitlab pipline"
    reviewResult = 1
    categoryName = "General App"
    #r = requests.post(url_, headers=headers_, data = json.dumps(data),verify=False)
    #print(r.text.encode('utf8')) 
    
    if resp['data'] != None :
        print(resp['data'])
        if resp['state'] == "success":
            print(resp['state'])
            send_approve(url_,resp['data'], reviewResult, categoryName,note)
            return None

    #print(resp['msg'])
    
    return resp['msg']
    
    
def upload_new_version_to_itms(url,source_file,packageName,rootAgency,versionDescription):
       
    payload = {'rootAgency': rootAgency,
                'packageName': packageName,
                'versionDescription': versionDescription
                }
     
    files = [
       ('apk', open(source_file,'rb'))
    ]
    
    headers = {
      #'Content-Type': 'multipart/form-data' ## command this header for solve for upload file
    }
    response = requests.request("POST", url, headers=headers, data = payload, files = files,verify=False)
         
    if response.status_code != requests.codes.ok:
        print("ResponseCode : ".format(errcode=response.status_code)) 
        return response.text.encode('utf8')
    
    resp = json.loads(str(response.text))

    url_ = "https://114.199.86.218:9443/iss/app/review"
    note = "Auto Approve by gitlab pipline"
    reviewResult = 1
    categoryName = "General App"

    if resp['data'] != None :
        print(resp['data'])
        if resp['state'] == "success":
            print(resp['state'])
            send_approve(url_,resp['data'], reviewResult, categoryName,note)
            return None
    
    #print(resp['msg'])
    return resp['msg']
    
    
def send_approve(url_,reviewId, reviewResult, categoryName,note):

    data = {"reviewId": reviewId,
            "categoryName": categoryName,
            "reviewResult": reviewResult,
            "note": note}
            
    headers_ = {
      'Content-Type': 'application/json'
    }    
    
    #url_ = "https://114.199.86.218:9443/iss/app/review"
    r = requests.post(url_, headers=headers_, data = json.dumps(data),verify=False)
    if r.status_code != requests.codes.ok:
        print("ResponseCode : ".format(errcode=r.status_code)) 
        return r.text.encode('utf8')
     
    resp = json.loads(str(r.text))
    #print(resp['msg'])
    return None
    
def delete_latest_version(url_,packageName, rootAgency):

    data = {"packageName": packageName,
            "rootAgency": rootAgency
            }
            
    headers_ = {
      'Content-Type': 'application/json'
    }    
    
    r = requests.post(url_, headers=headers_, data = json.dumps(data),verify=False)
    if r.status_code != requests.codes.ok:
        print("ResponseCode : ".format(errcode=r.status_code)) 
        return r.text.encode('utf8')
     
    #resp = json.loads(str(r.text))
    return None
    
def delete_apps(url_,packageName, rootAgency):

    data = {"packageName": packageName,
            "rootAgency": rootAgency
            }
            
    headers_ = {
      'Content-Type': 'application/json'
    }    
    
    r = requests.post(url_, headers=headers_, data = json.dumps(data),verify=False)
    if r.status_code != requests.codes.ok:
        print("ResponseCode : ".format(errcode=r.status_code)) 
        return resp['msg']
     
    #resp = json.loads(str(r.text))
    return None
    
def get_app(release_dir):
    '''Extract app data
    
    Args:
        release_dir (str): Path to release directory.

    Returns:
        (str, str): App version and path to release apk file.
    '''
    output_path = os.path.join(release_dir, 'output.json')

    with(open(output_path)) as app_output:
        json_data = json.load(app_output)

    apk_details_key = ''
    if 'apkInfo' in json_data[0]:
        apk_details_key = 'apkInfo'
    elif 'apkData' in json_data[0]:
        apk_details_key = 'apkData'
    else:
        print("Failed: parsing json in output file")
        return None, None

    app_version = json_data[0][apk_details_key]['versionName']
    app_file = os.path.join(release_dir, json_data[0][apk_details_key]['outputFile'])
    return app_version, app_file


   

if __name__ == '__main__':
    # Command line arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('--itms.action', dest='action', help='path to release folder', required=False)
    parser.add_argument('--itms.apk.dir', dest='apk_dir', help='path to release folder', required=False)
    parser.add_argument('--itms.apk.name', dest='apk_name', help='app name that will be used as file name', required=False)
    parser.add_argument('--itms.rootAgency', dest='rootAgency', help='path to changelog file', required=False)
    parser.add_argument('--itms.models', dest='models', help='path to email template file', required=False)
    parser.add_argument('--itms.ageNames', dest='ageNames', help='dropbox access token', required=False)
    parser.add_argument('--itms.appDescription', dest='appDescription', help='dropbox target folder', required=False)
    parser.add_argument('--itms.appType', dest='appType', help='zapier email web hook', required=False)
    parser.add_argument('--itms.screenShot', dest='screenShot', help='image apps', required=False)
    parser.add_argument('--itms.versionDescription', dest='versionDescription', help='itms_url', required=False)
    parser.add_argument('--itms.url', dest='itms_url', help='itms_url', required=False)    
    parser.add_argument('--itms.reviewId', dest='reviewId', help='itms_url', required=False)
    parser.add_argument('--itms.reviewResult', dest='reviewResult', help='itms_url', required=False)
    parser.add_argument('--itms.categoryName', dest='categoryName', help='itms_url', required=False)    
    parser.add_argument('--itms.note', dest='note', help='itms_url', required=False)
    parser.add_argument('--itms.packageName', dest='packageName', help='packageName', required=False)
    

    options = parser.parse_args()
        
    if "upload" == options.action:
        app_version, app_file = get_app(options.apk_dir)  
        if options.versionDescription != None :
            app_version = options.versionDescription            
        #print("test helmi")
        print(app_version)
        print(app_file)
        #print("test helmi")  
        res = upload_to_itms(options.itms_url,
                            app_file,
                            options.screenShot,
                            options.rootAgency,
                            options.models,
                            options.ageNames,
                            options.appDescription,
                            app_version,
                            options.appType)
        exit(res)
        
    if "approve" == options.action:
        res = send_approve(options.itms_url,options.reviewId, options.reviewResult, options.categoryName,options.note)
        if res != None:
            exit(res)
        
    if "delete" == options.action:
        res = delete_apps(options.itms_url,options.packageName, options.rootAgency)
        if res != None:
            exit(res)
        
    if "delete_latest" == options.action:
        res = delete_latest_version(options.itms_url,options.packageName, options.rootAgency)
        if res != None:
            exit(res)
        
    if "new_version" == options.action:
        app_version, app_file = get_app(options.apk_dir)  
        if options.versionDescription != None :
            app_version = options.versionDescription
        print(app_version)
        print(app_file)
        res = upload_new_version_to_itms(options.itms_url,
                            app_file,
                            options.packageName,
                            options.rootAgency,
                            app_version
                            )
        if res != None:
            exit(res)
        
    #exit()
    
    